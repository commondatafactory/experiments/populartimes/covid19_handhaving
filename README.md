# covid19_handhaving

Provide a data api for a dashboard based on
google popular times to detect "busy" area's
where possible "covidiot" intervention is needed.

This is the backend API + scraping code.
It was and is a hasty job.

multiple frontends are being developed.
they are password proteded though.

[drukte.commondatafacotry.nl](https://drukte.commondatafacotry.nl)

[places.focustest.nl](htts://places.focustest.nl)


# QUICKSTART.

create a python virtualenv

 `pip install -r requirements.txt`

optional check the github repositories used
and checkthem out locally and pip install -e those as well


# prepare database

`  docker-compose up postgres`

  eiher run `python models.py` to create empty models

  or restore database dump by putting the dump in rawdata folder
  and execute

  `docker-compose run postgres pg_restore -U covid19 -d covid19 /rawdata/thedump.gz`

# install current package editable

  `pip install -e .`

# run the API.

  `uvicorn popularplacesdata.api.places:app`

  `uvicorn popularplacesdata.api.places:app --reload` for development.

   or

   `docker-compose up api`

  set `GOOGLE_API_KEY` environment variable to enable adding places.

# add place_id's to database.

Use https://developers.google.com/places/web-service/place-id
to find them.

Add places to the `/scrape_place/{place_id}`  API endpoint. If a current
popularity is found. The place will imediatly show up on the maps.


  set `GOOGLE_API_KEY` environment variable.

   `python -m popularplacesdata.update`

   to start a scrape of known places which have a `current_popularity` values.

# NOTE.

    The `/popularplaces` endpoint returns geojson.
    should work well untill around 5000 locations. We should switch to a mapserver / vector tiles

# TESTS

    coverage run --source=populartimesdata -m pytest populartimesdata

    coverage report

    81% (22-07-2020)


