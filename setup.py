"""
Create a package from python code.

pip install -e .
"""
from setuptools import setup, find_packages
setup(
    name='pulartimesdata',
    version='0.0.1',
    packages=find_packages(),
    author='Stephan Preeker',
    author_email='stephan@preeker.net',
    description='Collect google popular times data',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.6',
        'License :: OSI Approved :: Mozilla Public License 2.0 (MPL 2.0)'
    ],

)
