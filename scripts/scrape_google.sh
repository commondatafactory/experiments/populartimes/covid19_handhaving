#!/usr/bin/env bash

set -u   # crash on missing env variables
set -e   # stop on any error
set -x   # print what we are doing


cd /home/ubuntu/covid19_handhaving

sudo -E docker-compose run --rm python python -m populartimesdata.update
