"""
A covidiot finding API.
"""
import os
import secrets
import logging
import datetime

from fastapi import Depends, FastAPI, HTTPException
from fastapi import Query, Response, status
from fastapi.middleware.cors import CORSMiddleware
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
from fastapi.responses import StreamingResponse
from fastapi.security import HTTPBasic, HTTPBasicCredentials

# from pydantic import BaseModel, Json

import populartimes

from populartimesdata import update
from populartimesdata.db import create_json_dump
from populartimesdata.db import db_helper
from populartimesdata.db import models
from populartimesdata.db import sql
from populartimesdata import settings

app = FastAPI()

logging.basicConfig(level=logging.DEBUG)
LOG = logging.getLogger(__name__)

origins = [
        "https://drukte.commondatafactory.nl",
        "https://places.focustest.nl",
        "http://127.0.0.1",
        "http://127.0.0.1:3000",
        "http://127.0.0.1:8080",
        "http://localhost",
        "http://localhost:3000",
        "http://localhost:8080",
]


app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

security = HTTPBasic()

log = logging.getLogger(__name__)


def get_current_username(
        credentials: HTTPBasicCredentials = Depends(security)):
    """Add basic auth to the api.
    """
    user = os.getenv('API_USER') or 'test'
    password = os.getenv('API_PASSWORD') or 'test'

    # when no environment variable is set the api is open!!
    if not user:
        raise Exception('auth not configured')

    if user == 'test' and not settings.TESTING['running']:
        raise Exception('test user while not testing')

    correct_username = secrets.compare_digest(credentials.username, user)
    correct_password = secrets.compare_digest(credentials.password, password)

    if not (correct_username and correct_password):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect email or password",
            headers={"WWW-Authenticate": "Basic"},
        )
    return credentials.username

@app.get('/')
def hello_world():
    """return instructions"""
    item = {
        'geojson popularity': "/popularplaces/",
        'place details': "/place/{place_id}?timestamp=OPTIONAL",
        'Add a place': "-> /scrape_place/{place_id}",
        'Find place_id': "-> https://developers.google.com/places/place-id",
        'All places': "/allplaces",
        'All places All time': "/allplacesalltime?before=OPTIONAL&after=OPTIONAL",  # noqa
    }
    json_compatible_item_data = jsonable_encoder(item)
    return JSONResponse(content=json_compatible_item_data)


def csv_stream(only_ignored_places=False):
    """return a csv stream"""
    stream = create_json_dump.all_places_csv(
        db_helper.get_session,
        only_ignored_places=only_ignored_places
    )
    response = StreamingResponse(
        iter([stream.getvalue()]),
        media_type="text/csv"
    )
    response.headers["Content-Disposition"] = "attachment; filename=places.csv"
    return response


@app.get('/allplaces')
async def all_places(
        _username: str = Depends(get_current_username),
        only_ignored_places: bool = False,
        exportformat: str = "geojson",
        ):
    """return all places!"""
    s = db_helper.get_session()
    if exportformat == "csv":
        s.commit()
        return csv_stream(
            only_ignored_places=only_ignored_places)

    new_json = create_json_dump.all_places_geojson(
        s, only_ignored_places=only_ignored_places)
    json_compatible_item_data = jsonable_encoder(new_json)
    s.commit()
    return JSONResponse(content=json_compatible_item_data)


@app.get('/allplacesalltime')
async def alldata(
        after: int = 1,  # seconds since epoch
        before: int = int(datetime.datetime.now().timestamp()),
        _username: str = Depends(get_current_username)):
    """return all data in csv format"""
    s = db_helper.get_session()
    stream = create_json_dump.all_places_all_time_csv(
        s, after, before)
    response = StreamingResponse(
        stream,
        media_type="text/csv"
    )
    response.headers["Content-Disposition"] = \
        "attachment; filename=places.ndjson"
    return response


@app.get('/popularplaces')
async def popular_places_geojson(
        response: Response,
        timestamp: int = None,  # seconds since epoch
        _username: str = Depends(get_current_username)):

    """create geojson of scraped popular places"""
    s = db_helper.get_session()
    new_geojson = create_json_dump.current_popularity_geojson(
        s, timestamp=timestamp)

    if not new_geojson:
        response.status_code = 404
        return {}
    s.commit()
    return JSONResponse(content=new_geojson)


def _get_place_config(place_id):
    db_model = models.ConfigPlaces
    rows = (
        db_helper.get_session().query(db_model)
        .filter_by(place_id=place_id)
        .order_by(db_model.created_at.desc())
        .limit(1)
    )
    configs = []
    for r in rows:
        configs.append(r)
    assert len(configs) < 2
    return configs


def _get_place_data(place_id, timestamp=None):
    """Request place id data from a current timestamp
    """
    timefrom = None
    defaulttime = datetime.datetime.now() + datetime.timedelta(days=1)
    if not timestamp:
        timefrom = defaulttime
    else:
        timefrom = datetime.datetime.fromtimestamp(timestamp)

    db_model = models.PopularTimesRaw
    rows = (
        db_helper.get_session().query(db_model)
        .filter_by(place_id=place_id)
        .filter(db_model.scraped_at < timefrom)
        .filter(db_model.scraped_at.isnot(None))
        .order_by(db_model.scraped_at.desc())
        .limit(100)
    )

    measurements = []
    data = {}

    for i, r in enumerate(rows):
        if i == 0:
            r.data['scraped_at'] = r.scraped_at.strftime('%Y-%m-%d %H:%M:%S')
            data = r.data

        pop = r.data.get('current_popularity')

        if pop is not None:
            measurements.append((int(r.scraped_at.timestamp()), pop))

    if measurements:
        data['measurements'] = measurements
        return data

    return {}


def _new_week_array():
    week_array = []
    for weekday in ['Monday', 'Tuesday', 'Wednesday',
                    'Thursday', 'Friday', 'Saturday', 'Sunday']:
        day_data = {
            'name': weekday,
            'data': list([-1 for i in range(24)])
        }
        week_array.append(day_data)
    return week_array


def _get_avg_data(place_id):
    """Create weekday array of measured data."""
    formatted_sql = sql.place_day_hour_avg.format(
        place_id=place_id)
    rs = db_helper.get_session().execute(formatted_sql)

    week_array = _new_week_array()

    for r in rs:  # noqa
        week_array[r.day]['data'][r.hour] = r.avgp

    return week_array


@app.get('/place/{place_id}')
def get_place(
        response: Response,
        _username: str = Depends(get_current_username),
        timestamp: int = None,
        place_id: str = Query(None, min_length=3, max_length=250)):
    """Get detailed place information. contains information from google and
       our own measured data
    """
    data = _get_place_data(place_id, timestamp=timestamp)
    db_helper.get_session().commit()

    if not data:
        response.status_code = 404
        return {"404": 'place is not scraped'}

    avg_week_array = _get_avg_data(place_id)
    data['measured_popularity'] = avg_week_array
    db_helper.get_session().commit()
    return data


@app.post('/ignore_place/{place_id}')
def ignore_place(
        response: Response,
        _username: str = Depends(get_current_username),
        place_id: str = Query(None, min_length=3, max_length=250),
        ignore: bool = True
        ):
    """
    Ingnore a place from further scraping
    """
    data = _get_place_data(place_id)

    if not data:
        response.status_code = 404
        return {"404": 'place is not scraped'}

    place_config = _get_place_config(place_id)
    old_config = {}
    if place_config:
        log.debug('found existing config')
        old_config = place_config[0].config

    s = db_helper.get_session()

    config = {}
    config.update(old_config)
    config['ignore'] = ignore

    nc = models.ConfigPlaces(
        place_id=place_id, config=config)
    s.add(nc)
    s.commit()

    return config


@app.get('/ignored')
def get_ignored(
        # response: Response,
        _username: str = Depends(get_current_username),
        ):
    """method to see what places are ignored"""
    return {'ignored': update.get_ignore_ids()}


@app.get('/scrape_place/{place_id}', status_code=200)
def scrape_place(
        response: Response,
        _username: str = Depends(get_current_username),
        place_id: str = Query(None, min_length=3, max_length=250),
        ):
    """get a place id to update.

    adds it to the items to check.
    """
    if not place_id:
        response.status_code = 400
        return "missing place_id"

    # start a update if key is set
    key = os.getenv('GOOGLE_API_KEY')
    if not key:
        response.status_code = 500
        return "GOOGLE API KEY NOT SET"

    try:
        m = update.scrape_single_place(key, place_id)
    except populartimes.crawler.PopulartimesException:
        m = None

    if not m:
        response.status_code = 500
        return f"no update results for {place_id}"

    p_score = m.get('current_popularity')
    p_name = m.get('name')

    if p_score:
        return f"location {p_name} should show up on map with {p_score} score"

    return f"location {p_name} does not have a current popularity score."
