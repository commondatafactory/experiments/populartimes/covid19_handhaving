"""
sql statements to get current/latest data
"""

latest = """
with
 houravg as (
 -- aggregate all places of past month for popularity statistics.
        select
        place_id,
        Max(a.scraped_at) as last_scraped_at,
        Count(id) as scrape_count,
        max(coalesce(data->>'current_popularity', '0')::int) as maxp,
        avg(coalesce(data->>'current_popularity', '0')::int)::int as avgp,
        stddev(coalesce(data->>'current_popularity', '0')::int)::int as stddevp
  from populartimes_raw a
  where
        a.scraped_at  >  CURRENT_TIMESTAMP - interval '1.5 month'
        and (
                extract('hour' from a.scraped_at) = extract('hour' from CURRENT_TIME)
        )
  group by a.place_id
), latest as (
        -- find the latest scraped items efficiently.
        SELECT distinct on (place_id)
                place_id,
                scraped_at,
                "data",
                coalesce(data->>'current_popularity', '0')::int current_pop
        FROM populartimes_raw r1
        where coalesce(data->>'current_popularity', '0')::int > 0
        and r1.scraped_at >  {time} at time zone 'utc' - interval '1.5 hours'
        and r1.scraped_at <  {time} at time zone 'utc' + interval '0.5 hours'
        ORDER BY place_id, scraped_at desc
),
previous as (
        -- find the previously scraped items efficiently. other time interval
        SELECT distinct on (r1.place_id)
                r1.place_id,
                r1.scraped_at,
                coalesce(data->>'current_popularity', '0')::int previous_pop
        FROM populartimes_raw r1
        where coalesce(data->>'current_popularity', '0')::int > 0
        and r1.scraped_at <  {time} at time zone 'utc' - interval '1.5 hours'
        and r1.scraped_at >  {time} at time zone 'utc' - interval '2.5 hours'
        ORDER BY place_id, scraped_at desc
)
-- select all recent places with statistics for that hour.
select
        l.place_id,
        current_pop,
        previous_pop,
        current_pop - previous_pop as diff_pop,
        scrape_count,
        maxp,
        avgp,
        stddevp,
        l.scraped_at,
        l."data"
from latest l
left outer join previous p on (l.place_id = p.place_id)  --new places have no previous
left outer join houravg h on (h.place_id = l.place_id)   --new places have no history
"""  # noqa


place_day_hour_avg = """
with
 houravg as (
        -- aggregate all places of past month for popularity statistics.
        select
        place_id,
        extract('dow' from a.scraped_at)::int as day,
        extract('hour' from a.scraped_at)::int as hour,
        Max(a.scraped_at) as last_scraped_at,
        Count(id) as scrape_count,
        max(coalesce(data->>'current_popularity', '0')::int) as maxp,
        avg(coalesce(data->>'current_popularity', '0')::int)::int as avgp,
        stddev(coalesce(data->>'current_popularity', '0')::int)::int as stddevp
  from populartimes_raw a
  where
        a.scraped_at  > current_timestamp - interval '1.5 month'
        and a.place_id = '{place_id}'
        --and coalesce(data->>'current_popularity', '0')::int > 0
  group by a.place_id, extract('hour' from a.scraped_at),
           extract('dow' from a.scraped_at)
)
select * from houravg
order by day, hour
"""   # noqa


all_places = """
with latestrecords as (
    --get last record of all places to get current location
    select distinct on (place_id)
    place_id,
    data->'name' as name,
    data->'address' as address,
    coalesce(data->'coordinates'->>'lat', '-1.0')::float as  lat,
    coalesce(data->'coordinates'->>'lng', '-1.0')::float as  lng
    from populartimes_raw
    order by place_id, scraped_at desc
), ignored as (
    select
        place_id,
        ignored_since
    from (
    select distinct on (place_id) place_id, config, created_at as ignored_since
    from configure_places
    order by place_id, created_at desc
    ) as latest_configs
    where coalesce(latest_configs.config->>'ignore')::bool
),
stats as (
    -- get some stats about place
    select
    pr.place_id,
    Max(scraped_at) as last_scraped_at,
    Count(id) as scrape_count,
    max(coalesce(data->>'current_popularity', '0')::int) as maxp,
    avg(coalesce(data->>'current_popularity', '0')::int)::int as avgp
from populartimes_raw pr
group by pr.place_id
)
select
    name, stats.place_id, last_scraped_at,
    scrape_count, maxp, avgp,
    address, lng, lat,
    ignored_since
from stats
left outer join latestrecords lr on (stats.place_id = lr.place_id)
left outer join ignored i on (i.place_id = stats.place_id)
{where}
order by scrape_count desc
"""
