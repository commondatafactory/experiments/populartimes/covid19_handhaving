"""
Define database tables.

- rawdata for orginal data

"""


import logging
import argparse
import asyncio
import datetime

from sqlalchemy import Column, Integer, TIMESTAMP, String
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.schema import Sequence
from geoalchemy2 import Geometry

# from aiopg.sa import create_engine as aiopg_engine
from . import db_helper

# logging.basicConfig(level=logging.DEBUG)
LOG = logging.getLogger(__name__)

Base = declarative_base()


async def create_tables(_args):
    """Main."""
    engine = db_helper.make_engine(section='docker')
    LOG.warning("CREATING DEFINED TABLES")
    # recreate tables
    Base.metadata.create_all(engine)
    # db_helper.alembic_migrate(engine)


class PopularTimesRaw(Base):
    """Raw populartimes API data."""

    __tablename__ = "populartimes_raw"
    id = Column(
        Integer, Sequence("popular_raw_sequence"),
        primary_key=True)
    place_id = Column(String, index=True)
    scraped_at = Column(TIMESTAMP, index=True)
    data = Column(JSONB)


class ConfigPlaces(Base):
    """ configure places.

    Save configuratation for each place.

    like:

    - ignore
    - allarm levels

    """
    __tablename__ = "configure_places"
    id = Column(
        Integer, Sequence("popular_ignore_sequence"),
        primary_key=True)
    place_id = Column(String, index=True)
    created_at = Column(
        TIMESTAMP, index=True, default=datetime.datetime.utcnow)
    config = Column(JSONB)


class PopularTimesMeasures(Base):
    """Raw measurements of google
    """
    __tablename__ = "populartimes_measurement"
    id = Column(
        Integer,
        Sequence("populartimes_measurement_sequence"),
        primary_key=True)  # Seq

    prio = Column(Integer, index=True)
    location = Column(String, index=True)
    neighborhood = Column(String, index=True)
    stadsdeel = Column(String(1), index=True)
    buurt_code = Column(String(4), index=True)
    geometrie = Column(Geometry('POINT', srid=4326), index=True)


if __name__ == "__main__":
    desc = "Create/Drop defined model tables."
    inputparser = argparse.ArgumentParser(desc)
    args = inputparser.parse_args()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(create_tables(args))
