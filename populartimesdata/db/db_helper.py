"""
DB utilities to create / delete (test) database  and tables
"""
import os
import logging

from sqlalchemy import create_engine
from sqlalchemy.engine.url import URL
from sqlalchemy.orm import sessionmaker
from sqlalchemy_utils.functions import database_exists
from sqlalchemy_utils.functions import create_database
from sqlalchemy_utils.functions import drop_database

from populartimesdata.settings import config_auth
from populartimesdata.settings import TESTING
from populartimesdata.settings import ENVIRONMENT_OVERRIDES


logging.basicConfig(level=logging.DEBUG)
LOG = logging.getLogger(__name__)
Session = sessionmaker()

session = []
engine = []


def make_conf(section, environment_overrides=None):
    """Create database connection."""

    if TESTING['running']:
        section = "test"

    logging.info('using db config section %s', section)

    db = {
        'host': config_auth.get(section, "host"),
        'port': config_auth.get(section, "port"),
        'database': config_auth.get(section, "dbname"),
        'username': config_auth.get(section, "user"),
        'password': config_auth.get(section, "password"),
    }

    if not environment_overrides:
        environment_overrides = ENVIRONMENT_OVERRIDES

    # override defaults with environment settings
    for var, env in environment_overrides:
        if isinstance(env, list):
            for e in env:
                if os.getenv(e):
                    db[var] = os.getenv(e)
        elif os.getenv(env):
            db[var] = os.getenv(env)

    conf = URL(
        drivername="postgresql",
        username=db['username'],
        password=db['password'],
        host=db['host'],
        port=db['port'],
        database=db['database'],
    )

    host, port, name = db['host'], db['port'], db['database']
    LOG.info("Database config %s:%s:%s", host, port, name)
    return conf


def create_db(section="test", environment=None):
    """Create the database."""
    conf = make_conf(section, environment_overrides=environment)
    LOG.info("Created database")
    if not database_exists(conf):
        create_database(conf)


def drop_db(section="test", environment=None):
    """Cleanup test database."""
    LOG.info("Drop database")
    conf = make_conf(section, environment_overrides=environment)
    if database_exists(conf):
        drop_database(conf)


def make_engine(section="test", environment=None):
    """create configuration with environment overrides"""
    conf = make_conf(section, environment_overrides=environment)
    global engine
    engine = create_engine(conf)
    return engine


def set_session(engine):
    """set a global session object to be reused"""
    global session
    Session.configure(bind=engine)
    # create a configured "session" object for tests
    session = Session()
    return session


def get_connection():
    """create a special connection """
    connection = make_engine(section="docker").connect()
    return connection


def get_session():
    """session"""
    if not session:
        setup_db()
    return session


def setup_db():
    """setup database connection"""

    if session:
        return session

    _engine = make_engine(section="docker")
    db_session = set_session(_engine)
    return db_session
