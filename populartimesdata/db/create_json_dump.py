"""
Execute sql to get latest of timestamp based information.
db layer of api

run standalone to
- Create a json dump of the latest places data

- test and create new json fixtures
"""

import json
import datetime
import logging
import io
import csv

from geojson import FeatureCollection, Feature, Point
# from starlette.responses import StreamingResponse

from populartimesdata.db import db_helper
from populartimesdata.db import sql
from populartimesdata import update


logging.basicConfig(level=logging.DEBUG)
LOG = logging.getLogger(__name__)


def _date_condition(d: datetime.datetime):
    """create a sql statement that loads data around time x"""

    around = "now()"

    if d:
        t = datetime.datetime.fromtimestamp(d)
        around = t.strftime('%Y-%m-%d %H:%M:%S')
        around = f"'{around}'"

    return around


def get_latest_rows(db_session, timestamp=None):
    """Get the latest popular times by timestamp
    """
    time = _date_condition(d=timestamp)
    formatted_sql = sql.latest.format(time=time)
    rs = db_session.execute(formatted_sql)
    return rs


def get_all_unique_places(db_session, only_ignored_places=False):
    """Get all places as geojson
    """
    where = ""
    if only_ignored_places:
        where = "WHERE ignored_since is not null"
    sqlstmt = sql.all_places.format(where=where)
    rs = db_session.execute(sqlstmt)
    return rs


def all_places_csv(db_session, only_ignored_places=False):
    """return all places in easy to use csv
    """
    writer_file = io.StringIO()
    outcsv = csv.writer(writer_file, delimiter='\t')
    # manual dataset
    rs = get_all_unique_places(db_session, only_ignored_places)
    outcsv.writerow(rs.keys())
    outcsv.writerows(rs.fetchall())
    # db_session.commit()
    return writer_file


def _tosqldate(s):
    """create sql date format"""
    t = datetime.datetime.fromtimestamp(s)
    sqlt = t.strftime('%Y-%m-%d %H:%M:%S')
    sqlt = f"'{sqlt}'"
    return sqlt


def all_places_all_time_csv(_, after=1, before=None):
    """load all data"""

    conn = db_helper.get_connection()
    after = _tosqldate(after)
    before = _tosqldate(before)

    proxy = (
        conn.execution_options(stream_results=True)
        .execute(
            f"""select * from populartimes_raw
                where scraped_at > {after}
                and scraped_at < {before}
                order by scraped_at desc
             """)
    )
    return results_generator(proxy)


async def results_generator(proxy):
    """bactch results"""

    while True:
        batch = proxy.fetchmany(1000)

        if not batch:
            break

        for row in batch:
            srow = json.dumps((
                row['place_id'],
                row['scraped_at'].timestamp(),
                row['data']
            ))
            yield f'{srow}\n'

    proxy.close()


def all_places_geojson(db_session, only_ignored_places=False):
    """
    Fetch latest scrape results
    """
    points = []

    # manual dataset
    rs = get_all_unique_places(db_session, only_ignored_places)

    for r in rs:
        y = r.lat
        x = r.lng
        p = Point((x, y))

        props = {'place_id': r.place_id}
        props['last_scraped_at'] = r.last_scraped_at.timestamp()
        props['scrape_count'] = r.scrape_count
        props['max_p'] = r.maxp
        props['avg_p'] = r.avgp
        props['ignored_since'] = r.ignored_since

        f = Feature(geometry=p, properties=props, id=r.place_id)
        points.append(f)

    c = FeatureCollection(
        points,
        total_places=len(points),
    )

    return c


def current_popularity_geojson(db_session, timestamp=None):
    """return geojson with places with a current popularity"""

    rs = get_latest_rows(db_session, timestamp=timestamp)

    points = []
    scraped_at = None

    ignored_places = update.get_ignore_ids()

    for i, r in enumerate(rs):
        if i == 0:
            scraped_at = r.scraped_at

        if r.place_id in ignored_places:
            continue

        y = r.data['coordinates']['lat']
        x = r.data['coordinates']['lng']
        p = Point((x, y))

        props = {
            'scraped_at': r.scraped_at.timestamp(),
            'scrape_count': r.scrape_count,
            'max_p': r.maxp,
            'avg_p': r.avgp,
            'std_p': r.stddevp,
            'current_popularity': r.current_pop,
            'previous_popularity': r.previous_pop,
            'diff_popularity': r.diff_pop,

            'name': r.data['name'],
            'address': r.data['address'],
            'rating': r.data['rating'],
            'types': r.data['types'],
            'id': r.place_id,
        }

        f = Feature(geometry=p, properties=props, id=r.place_id)
        points.append(f)

    if not scraped_at:
        return {}

    c = FeatureCollection(
        points,
        scraped_at=scraped_at.timestamp(),
        _scraped_at=scraped_at.strftime("%Y-%m-%d %H:%M"),
    )

    return c


def create_dump():
    """create new fresh json dump of latest places data"""

    db_session = db_helper.setup_db()

    def _size(j):
        return len(current_places.get('features', []))

    with open('fresh.json', 'w') as outfile:
        current_places = current_popularity_geojson(db_session)
        LOG.debug('writing file with %d features', _size(current_places))
        json.dump(current_places, outfile, indent=2, sort_keys=True)

    # tests.
    current_places = current_popularity_geojson(
        db_session,
        timestamp=1586960300
    )
    LOG.debug('%d features', _size(current_places))

    # tests.
    current_places = current_popularity_geojson(
        db_session,
        timestamp=1086960300
    )
    LOG.debug('%d features', _size(current_places))
    assert _size(current_places) == 0

    current_places = get_latest_rows(
        db_session,
        timestamp=1586960300
    )

    LOG.debug('%d features', len(current_places['places']))

    current_places = current_popularity_geojson(db_session)
    LOG.debug('found %d features @ now', _size(current_places))


if __name__ == '__main__':
    create_dump()
