"""
pip install the module

then execute this script with

python -m scrape.load_populartimes

"""

import glob
import json
import os
import logging
from pathlib import Path

import dateparser

from . import db_helper
from . import models
from . import settings


logging.basicConfig(level=logging.DEBUG)
LOG = logging.getLogger(__name__)

path = Path(settings.BASE_DIR)

print(path)
print(os.path.join(str(path.parent), 'rawdata', '*.json'))

# get the session
engine = db_helper.make_engine()
db_session = db_helper.set_session(engine)

json_files = os.path.join(str(path.parent), 'rawdata', 'data', '*.json')

for f in glob.glob(json_files):
    date = f.split('/')[-1]
    date = date.split('.')[0]
    date = date.replace('_', ' ')

    scraped_at = dateparser.parse(date)

    with open(f) as j:
        scrape = json.load(j)

    objects = []
    # session.query(User).filter_by(name='jack'):
    # skip file if record with that date exists.
    c = db_session.query(models.PopularTimesRaw).filter_by(scraped_at=scraped_at).count()
    if c > 0:
        print('already loaded %s with %d records', f, c)
        print(c)
        continue

    for p in scrape['places']:
        grj = dict(
            place_id=p['id'],
            scraped_at=scraped_at,
            data=p)
        objects.append(grj)

    db_model = models.PopularTimesRaw
    db_session.bulk_insert_mappings(db_model, objects)
    db_session.commit()
