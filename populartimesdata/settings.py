"""
populartimesdata settings
"""
import os
import configparser
from sqlalchemy.ext.declarative import declarative_base

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

config_auth = configparser.ConfigParser()
config_auth.read(os.path.join(BASE_DIR, "config.ini"))

Base = declarative_base()

TESTING = {"running": False}

ENVIRONMENT_OVERRIDES = [
    ('host', ['DATABASE_HOST', 'CDF_CLUSTER_PORT_5432_TCP_ADDR']),
    ('port', ['DATABASE_PORT', 'CDF_CLUSTER_SERVICE_PORT_POSTGRESQL']),
    ('database', 'DATABASE_NAME'),
    ('username', 'DATABASE_USER'),
    ('password', 'DATABASE_PASSWORD'),
]
