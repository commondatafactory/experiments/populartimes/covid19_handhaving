/*
	orignal query before we add variables in python.
*/
with
 houravg as (
 -- aggregate all places of past month for popularity statistics.
        select
        place_id,
        Max(a.scraped_at) as last_scraped_at,
        Count(id) as scrape_count,
        max(coalesce(data->>'current_popularity', '0')::int) as maxp,
        avg(coalesce(data->>'current_popularity', '0')::int)::int as avgp,
        stddev(coalesce(data->>'current_popularity', '0')::int)::int as stddevp
  from populartimes_raw a
  where
        a.scraped_at  >  CURRENT_TIMESTAMP - interval '1.5 month'
        and (
        	extract('hour' from a.scraped_at) = extract('hour' from CURRENT_TIME)
        )
  group by a.place_id
), latest as (
	-- find the latest scraped items efficiently.
	SELECT distinct on (place_id)
		place_id, scraped_at, "data", coalesce(data->>'current_popularity', '0')::int current_pop
	FROM populartimes_raw r1
	where coalesce(data->>'current_popularity', '0')::int > 0
	and r1.scraped_at >  now() at time zone 'utc' - interval '1.5 hours'
	ORDER BY place_id, scraped_at desc
),
previous as (
	-- find the previously scraped items efficiently. other time interval
	SELECT distinct on (r1.place_id)
		r1.place_id, r1.scraped_at,  coalesce(data->>'current_popularity', '0')::int previous_pop
	FROM populartimes_raw r1
	where coalesce(data->>'current_popularity', '0')::int > 0
	and r1.scraped_at <  now() at time zone 'utc' - interval '1.5 hours'
	and r1.scraped_at >  now() at time zone 'utc' - interval '2.5 hours'
	ORDER BY place_id, scraped_at desc
)
-- select all recent places with statistics for that hour.
select
        l.place_id,
        current_pop,
        previous_pop,
        current_pop - previous_pop as diff_pop,
        scrape_count,
        maxp,
        avgp,
        stddevp,
        l.scraped_at,
        l."data"
from latest l
left outer join previous p on (l.place_id = p.place_id)  --new places have no previous
left outer join houravg h on (h.place_id = l.place_id)   --new places have no history
