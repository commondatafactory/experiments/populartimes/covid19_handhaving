"""
Some tests.
"""

import os
import datetime
import json
import unittest
import logging
from datetime import timezone

from unittest import mock

from fastapi.testclient import TestClient
from requests.auth import HTTPBasicAuth


from populartimesdata.settings import BASE_DIR
from populartimesdata.settings import TESTING
from populartimesdata import update
from populartimesdata.db import models
from populartimesdata.db import db_helper
from populartimesdata import api


log = logging.getLogger(__name__)

db_vars = {
    'connection': [],
    'transaction': []
}


def setup_module():
    """setup a test database"""
    log.debug('setup test db')
    TESTING["running"] = True
    db_helper.drop_db()
    db_helper.create_db()
    engine = db_helper.make_engine(section="test")
    connection = engine.connect()
    db_vars['transaction'] = connection.begin()
    db_vars['connection'] = connection
    db_vars['engine'] = engine
    session = db_helper.set_session(engine)
    session.execute("CREATE EXTENSION if not exists postgis;")
    session.commit()
    models.Base.metadata.drop_all(bind=engine)
    models.Base.metadata.create_all(bind=engine)
    load_fixture_raw()


def teardown_module():
    """clean up after testing"""
    log.debug('cleanup test db')
    db_vars['transaction'].rollback()
    db_helper.session.close()
    db_vars['engine'].dispose()
    db_vars['connection'].close()
    db_helper.drop_db()
    TESTING["running"] = False


def mock_get_id():
    """mock getting a json object"""
    places = _get_test_fixture()
    test_places = []
    for p in places['places']:
        test_places.append(p)
    return test_places


def _get_test_fixture():
    test_file = ["tests", "locations", "testlocations.json"]
    test_file_path = os.path.join(BASE_DIR, *test_file)
    with open(test_file_path) as detail_json:
        places_json = json.loads(detail_json.read())
        return places_json
    return {}


def load_fixture_raw():
    """load fixtures
    """
    fixture = _get_test_fixture()
    scraped_at = datetime.datetime.utcnow()
    c = (
        db_helper.session.query(models.PopularTimesRaw)
        .filter_by(scraped_at=scraped_at).count()
    )

    if c > 0:
        log.info('already loaded with %d records', c)
        return

    objects = []

    for p in fixture['places']:
        grj = dict(
            place_id=p['id'],
            scraped_at=datetime.datetime.now(timezone.utc),
            data=p)
        objects.append(grj)

    db_model = models.PopularTimesRaw
    db_helper.session.bulk_insert_mappings(db_model, objects)
    db_helper.session.commit()


client = TestClient(api.places.app)


class TestDBWriting(unittest.TestCase):
    """
    Test writing to database

    update looks at locations that have been loaded before
    and have a popularity in the past month

    starts doing an update so we load in one location
    and see if update will update 1 location
    """

    @mock.patch("populartimes.get_id")
    def test_get_place(self, get_json_mock):
        """test loading of a place in db
        """
        log.debug('running a test database')
        get_json_mock.side_effect = mock_get_id()
        count = db_helper.session.query(models.PopularTimesRaw).count()
        self.assertEqual(count, 286)
        update.start_import(workers=1, make_engine=False)
        count = db_helper.session.query(models.PopularTimesRaw).count()
        self.assertEqual(count, 572)


def test_read_main():
    """index happy"""
    response = client.get("/")
    assert response.status_code == 200


def test_auth():
    """test basic auth"""
    auth_urls = [
        '/allplaces',
        '/allplacesalltime',
        '/popularplaces',
        '/place/x',
    ]
    for url in auth_urls:
        response = client.get(url)
        assert response.status_code == 401

    post_auth_urls = [
        '/ignore_place/x'
    ]
    for url in post_auth_urls:
        response = client.post(url)
        assert response.status_code == 401


def test_allplaces():
    """testing all places get"""

    # ignore lidl
    lidl = 'ChIJefDoqqTjxUcRM6OnpUZbH2Y'
    client.post(
        f'/ignore_place/{lidl}',
        auth=HTTPBasicAuth('test', 'test'))

    # get all places
    response = client.get('/allplaces', auth=HTTPBasicAuth('test', 'test'))
    data = response.json()
    assert response.status_code == 200
    # make sure we got everything
    assert len(data['features']) == 286
    # check if lidl has a ignored_since.
    seenlidl = False
    for i in data['features']:
        if i['id'] == lidl:
            assert i['properties']['ignored_since'] is not None
            seenlidl = True
        else:
            assert i['properties']['ignored_since'] is None

    assert seenlidl

    # get one ignored place
    response = client.get(
        '/allplaces',
        params={'only_ignored_places': True},
        auth=HTTPBasicAuth('test', 'test'))

    data = response.json()

    assert len(data['features']) == 1

    # cleanup
    # un-ignore a place
    response = client.post(
        f'/ignore_place/{lidl}?ignore=0',
        auth=HTTPBasicAuth('test', 'test'))


def test_popularplaces():
    """testing all places get"""
    response = client.get(
        '/popularplaces',
        auth=HTTPBasicAuth('test', 'test'))
    assert response.status_code == 200
    data = response.json()
    assert len(data['features']) == 286


def test_allplacesalltime():
    """testing all places get"""
    response = client.get(
        '/allplacesalltime', auth=HTTPBasicAuth('test', 'test'))
    assert response.status_code == 200


def test_getplace():
    """testing all places get"""
    response = client.get(
        '/place/ChIJefDoqqTjxUcRM6OnpUZbH2Y',
        auth=HTTPBasicAuth('test', 'test'))
    assert response.status_code == 200
    data = response.json()
    assert data['name'] == 'Lidl'


def test_scrape_place():
    """scraping of place should fail since we have no api key"""
    response = client.get(
        '/scrape_place/ChIJefDoqqTjxUcRM6OnpUZbH2Y',
        auth=HTTPBasicAuth('test', 'test'))
    assert response.status_code == 500


def test_ignore():
    """ingore a place

    # should no longer be-scraped
    # should no longer show-up in popular places

    # should be able to un-ignore and show up again
    """
    # test invalid place_id
    response = client.post(
        '/ignore_place/mistake',
        auth=HTTPBasicAuth('test', 'test'))
    assert response.status_code == 404

    # ignore lidl
    lidl = 'ChIJefDoqqTjxUcRM6OnpUZbH2Y'
    response = client.post(
        f'/ignore_place/{lidl}',
        auth=HTTPBasicAuth('test', 'test'))
    assert response.status_code == 200

    # test lidl being ignored
    ignored = update.get_ignore_ids()
    assert lidl in ignored
    response = client.get(
        '/ignored',
        auth=HTTPBasicAuth('test', 'test'))
    data = response.json()
    assert lidl in data['ignored']

    # test lidl not showing up in popular places
    response = client.get(
        '/popularplaces',
        auth=HTTPBasicAuth('test', 'test'))

    data = response.json()
    feature_ids = [f['properties']['id'] for f in data['features']]
    assert lidl not in feature_ids

    # un-ignore a place
    response = client.post(
        f'/ignore_place/{lidl}?ignore=0',
        auth=HTTPBasicAuth('test', 'test'))
    assert response.json()['ignore'] is False
    ignored = update.get_ignore_ids()
    assert lidl not in ignored
