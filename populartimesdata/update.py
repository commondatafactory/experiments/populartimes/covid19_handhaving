"""Save external popular times API measurements.

Ment to run in a conjob

Will work fine for around 3600 places an hour then the popularplaces
libary needs to be made concurrent. But that would blow the google api
api key cost out of proportion
"""

import logging
import os
import asyncio
import argparse
# import aiohttp
import time
# import json
import datetime
from datetime import timezone
import populartimes

from populartimesdata.db import db_helper
from populartimesdata.db import models

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)

logging.getLogger("urllib3").setLevel(logging.ERROR)
logging.getLogger("crawler").setLevel(logging.ERROR)

WORKERS = 1
STATUS = {"done": False}


api_config = {
    "api_key": os.getenv("GOOGLE_API_KEY", ""),
}

PLACE_QUEUE = asyncio.Queue()
RESULT_QUEUE = asyncio.Queue()


def add_items_to_db(pop_measurements):
    """Store json to database."""
    if not pop_measurements:
        log.error("No data recieved")
        return

    # Store the location json!
    scraped_at = datetime.datetime.now(timezone.utc)
    place_id = pop_measurements['id']

    # get the session
    db_session = db_helper.session

    grj = dict(
        place_id=place_id,
        scraped_at=scraped_at,
        data=pop_measurements)

    objects = []
    db_model = models.PopularTimesRaw
    objects.append(grj)

    db_session.bulk_insert_mappings(db_model, objects)
    db_session.commit()


async def store_results():
    """Store json in database."""

    counter = 0

    while True:
        measurements = await RESULT_QUEUE.get()

        if measurements == "terminate":
            break

        counter += 1
        add_items_to_db(measurements)
        log.debug("Stored %d measurements", counter)


async def log_progress():
    """ show progres every 10 seconds"""
    while True:
        size = PLACE_QUEUE.qsize()
        log.info("Progress %s", size)
        await asyncio.sleep(10)


past_month_places_with_popularity = """
    select place_id
    from populartimes_raw pr
    where scraped_at  >  (CURRENT_TIMESTAMP - interval '1.5 month')
    and coalesce(data->>'current_popularity', '0')::int > 0
    group by place_id
    order by max(coalesce(data->>'current_popularity', '0'))  desc
"""

past4hours = """
select place_id, max(coalesce(data->>'current_popularity', '0'))
      from populartimes_raw pr
      where  scraped_at  > NOW() - INTERVAL '4 HOURS'
      and coalesce(data->>'current_popularity', '0')::int > 20
      group by place_id
      order by max(coalesce(data->>'current_popularity', '0')::int) desc
"""

ignore_ids_sql = """
select * from (
select distinct on (place_id) place_id, config, created_at
from configure_places
order by place_id, created_at desc
) as latest_configs
where coalesce(latest_configs.config->>'ignore')::bool
"""


def _get_relevant_places(rows, sql):
    # get the session
    db_session = db_helper.get_session()
    # manual dataset
    rs = db_session.execute(sql)
    db_session.commit()

    for place in rs:
        rows.append(place)


def get_ignore_ids():
    """list ignored places
    """
    db_session = db_helper.get_session()
    rs = db_session.execute(ignore_ids_sql)
    db_session.commit()
    ignore_place_ids = [r[0] for r in rs]
    return set(ignore_place_ids)


async def fill_places_queue(workers):
    """Decide what to pick up.
    """
    rows = []

    _get_relevant_places(rows, past4hours)
    _get_relevant_places(rows, past_month_places_with_popularity)

    log.info("places with recent popularity %d", len(rows))

    place_ids = [r[0] for r in rows]
    ignore_ids = get_ignore_ids()

    # unique ids
    place_ids = set(place_ids) - ignore_ids

    for i, p_id in enumerate(place_ids):
        if i > 1800:
            break
        await PLACE_QUEUE.put(p_id)

    # Terminate instructions for all workers
    for _ in range(workers):
        await PLACE_QUEUE.put("terminate")


async def load_popularity(work_id):
    """do requests to google.
    """
    api_key = api_config['api_key']

    while True:
        place_id = await PLACE_QUEUE.get()

        if place_id == 'terminate':
            break

        # just to be friendly.
        # for other asyncio routines
        # and for the google api.
        await asyncio.sleep(0.01)

        # make this a a asyncio friendly function.
        try:
            r = populartimes.get_id(api_key, place_id)
        except Exception as e:
            # we want the scraper to never stop on any error
            log.exception(place_id)
            log.exception(e)
            log.error('google api failing? %s %s', work_id, place_id)
            continue

        await RESULT_QUEUE.put(r)


def scrape_single_place(api_key, place_id):
    """scrape a single place id

    To add new places to the scraper via the api.
    """
    measurement = populartimes.get_id(api_key, place_id)
    add_items_to_db(measurement)
    return measurement


async def run_workers(workers=WORKERS):
    """Run X workers processing fetching tasks."""
    # start job of puting data into database
    store_data = asyncio.ensure_future(store_results())
    # select places to pick up.
    add_place_ids = asyncio.ensure_future(fill_places_queue(workers))

    progress = asyncio.ensure_future(log_progress())

    log.info("Starting %d workers", workers)

    workers = [
        asyncio.ensure_future(load_popularity(i))
        for i in range(workers)]

    # add place ids on all
    print('places')
    await asyncio.gather(add_place_ids)
    # wait till workers are done
    print('workers')
    await asyncio.gather(*workers)
    await RESULT_QUEUE.put("terminate")
    # WAIT till storing of data is done.
    print('store stuff')
    await asyncio.gather(store_data)
    progress.cancel()

    # wait untill all data is stored in database

    # request all details of list
    log.debug("done!")


async def main(workers=WORKERS, make_engine=True):
    """create db engine, start workers"""
    # when testing we do not want an engine.
    if make_engine:
        engine = db_helper.make_engine(
            section="docker")
        db_helper.set_session(engine)

    await run_workers(workers=workers)


def start_import(workers=WORKERS, make_engine=True):
    """start main event loop"""
    start = time.time()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(
        main(workers=workers, make_engine=make_engine))

    log.info("Took: %s", time.time() - start)


if __name__ == "__main__":

    desc = "Scrape Google PLACES API."
    inputparser = argparse.ArgumentParser(desc)

    inputparser.add_argument(
        "--debug", action="store_true", default=False, help="Enable debugging"
    )

    args = inputparser.parse_args()
    if args.debug:
        # loop.set_debug(True)
        log.setLevel(logging.DEBUG)
    start_import()
